import logo from './logo.svg';
import './App.css';
import Form from './components/Form';

function App() {
  return <Form title='App' />;
}

export default App;
