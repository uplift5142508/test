import React, { useState } from 'react';

function Form(props) {
  // let title = props.title;
  const [title, setTitle] = useState(props.title);
  const [detectChange, setDetectedChange] = useState(false);

  const clickHandler = () => {
    // title = 'Updated';
    // console.log(title);
    setTitle('Updated');
    setDetectedChange((previousState) => !previousState);
  };

  return (
    <form>
      <h1>{title}</h1>
      <p>Did the value change? {detectChange ? 'Yes' : 'No'} </p>
      <button onClick={clickHandler} type='button'>
        Change Title
      </button>
    </form>
  );
}

export default Form;
